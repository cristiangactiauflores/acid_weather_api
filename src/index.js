import express from 'express';
import bodyParser from 'body-parser';
import morgan from 'morgan';
import cors from 'cors';
import weatherRouter from './routes/weather'
import redis from 'redis';


let app = express();

// settings
app.set('port', process.env.port || 8000)
app.set('json spaces', 2);

//middlewares
app.use(cors())
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json());

// Routes
app.use('/api/weather', weatherRouter);

app.listen(process.env.PORT || 8000, () => {
    console.log("server running")
})
