import {Router} from 'express';
import {getWeatherByCountryName} from '../controllers/weathersController'
let router = Router();

//Routes
router.get('/', getWeatherByCountryName);

export default router;