import request from 'request'
import redis from 'redis'

//Initialize redis
const client = redis.createClient(process.env.REDISCLOUD_URL, {no_ready_check: true});

function getSkyInformation(lat,lng,countryName) {
    return new Promise((resolve, reject) => {
        //Asks if there is already an object with the unique key
        //if it does not exist, it makes a call to the api of darksky to obtain the information of the country
        //and in case the key exists, the value will be searched directly from redis.
        client.hgetall(countryName, function(err, obj) {
            if(!obj) {
                let weatherResponse = {}
                let currentSeasson = 0;
                request('https://api.darksky.net/forecast/ab9bd971345ec02c7417e78b62aeae84/'+ lat +','+ lng + '?units=si&exclude=hourly,minutely,daily&lang=es', { json: true }, (err, res, body) => {
                    if(res.statusCode != 200) {
                        reject({
                            message: 'Error de la aplicación',
                            status: 500
                        })
                    }
                    currentSeasson = getCurrentSeason(lat)
                    weatherResponse.temperature = body.currently.temperature;
                    weatherResponse.summary = body.currently.summary;
                    weatherResponse.currentSeasson = currentSeasson;
                    client.hmset(countryName,[
                        'temperature', weatherResponse.temperature,
                        'summary', weatherResponse.summary,
                        'currentSeasson', weatherResponse.currentSeasson
                    ])
                    resolve(weatherResponse);
                })
            } else {
                client.hgetall(countryName, function(err, obj) {
                    resolve(obj)
                })
            }
        })
    })
}

//Get current season by latitude
//latitude is important to determine the 2 hemispheres
function getCurrentSeason(latitude){
    const currentMonth = new Date().getMonth() + 1;
    let season = ''
    switch(currentMonth) {
        case 12:
        case 1:
        case 2:
            season = latitude < 0 ? 'Verano': 'Invierno'
            return season
        break;
        case 3:
        case 4:
        case 5:
            season =  latitude < 0 ? 'Otoño': 'Primavera';
            return season
        break;
        case 6:
        case 7:
        case 8:
            season = latitude < 0 ? 'Invierno': 'Verano';
            return season
        break;
        case 9:
        case 10:
        case 11:
            season = latitude < 0 ? 'Primavera': 'Otoño';
            return season
        break;
    }
}



module.exports = {
    getSkyInformation
}