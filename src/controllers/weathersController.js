import DarkSky from '../models/darksky';

export function getWeatherByCountryName(req, res) {
    DarkSky.getSkyInformation(req.query.lat, req.query.lng, req.query.countryName)
        .then((response) => {
            res.send(response)
        }).catch(err => {
            if (err.status) {
                res.status(err.status).json({ message: err.message })
            } else {
                res.status(500).json({ message: err })
            }
         })
}